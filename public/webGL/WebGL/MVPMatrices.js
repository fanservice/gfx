let pMatrix = mat4.create();
let mVMatrix = mat4.create();
let Normal = mat3.create();
let DiffuseColor = [1,1,1];
let SpecularColor = [1,1,1];
let LightPos =[0,10,0];
let uMode=0;


//
let Kd = 1.0;
let Ks= 1.0;
let Shininess = 50.0;


function setMatrixUniforms() {
    //mat4

    gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
    gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mVMatrix);
    //mat4
    gl.uniformMatrix3fv(shaderProgram.NormalMatrixUniform, false, Normal);
    //vec
    gl.uniform3fv(shaderProgram.uDiffuseColorUniform,DiffuseColor);
    gl.uniform3fv(shaderProgram.uSpecularColorUniform,SpecularColor);
    gl.uniform3fv(shaderProgram.uLightPosrUniform,LightPos);
    //floats
    gl.uniform1f(shaderProgram.ukdUniform,Kd);
    gl.uniform1f(shaderProgram.uksUniform,Ks);
    gl.uniform1f(shaderProgram.uShininessValUniform,Shininess);
    //int
    gl.uniform1i(shaderProgram.uMode,uMode);


}
