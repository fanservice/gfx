//Magic Number
//0=Buffer
//TODO 1=Texture
let myObjects = {
    "objects":
        [],
};
let cameraTransformation = {};
let worldTransformations = {};
let globalM = [];
let cameraMatrix = [];
let lightTransfomrations = {};

//all globals for debugging in browser


function webGlInit() {

    Promise.resolve(importObjects("/obj/torso"))
        .then((objects) => {
            //World
            worldTransformations = new World("world", [0, 0, 0]);
            cameraTransformation = new Camera('camera', [0, 0, 5]);
            lightTransfomrations = new Light('Light', [0, 10, 0]);
            //setIdentity of everything

            let canvas = document.getElementById("webGLCanvas");
            initGL(canvas);
            initShaders();
            initTexture();
            //clear color aso
            gl.clearColor(0.0, 0.0, 0.0, 1.0);
            //enable depth for camera
            gl.enable(gl.DEPTH_TEST);
            //event handling
            document.onkeyup = handleKeyUp;
            document.onkeydown = handleKeyDown;

            myObjects.objects[0] = new Torso(0, objects, [0, 0, 0]);
            //call drawing
        }).then(() => {
        tick()
    })
}

function tick() {
    //build in javascript function for pausing brwoser
    requestAnimationFrame(tick);
    //wait for texture
    if (Textureloaded) {
        //self explaining
        handleKeys();
        //draw it
        drawScene();
    } else {
        console.log("Texture is " + Textureloaded + " texture not loaded")
    }
}