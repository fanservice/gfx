//setIdentity shaders and get
// source = http://learningwebgl.com/blog/?p=28
//source 2= http://learningwebgl.com/blog/?page_id=1217
//Magic Number Shaders:
//0: Garoud
//1: Phong
let shaderProgram;
let ShaderChooser = 0;
//get the shaders which is defined in folder shader and return it to js

function getShader(gl, shaderCode) {

    let shader;
    if (shaderCode.name === "fs") {
        shader = gl.createShader(gl.FRAGMENT_SHADER);
    } else if (shaderCode.name === "vs") {
        shader = gl.createShader(gl.VERTEX_SHADER);
    } else {
        alert("no shader found?");
        return null;
    }
    gl.shaderSource(shader, shaderCode.shader);
    gl.compileShader(shader);

    if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
        alert(gl.getShaderInfoLog(shader));
        return null;
    }
    return shader;
}


//setIdentity the shaders and attach
function initShaders() {
    let fragmentShader;
    let vertexShader;
    if (ShaderChooser === 0) {
        fragmentShader = getShader(gl, shaders.FGaroudShader);
        vertexShader = getShader(gl, shaders.VGaroudShader);
    }else if(ShaderChooser === 1)
    {
        fragmentShader = getShader(gl, shaders.FPhongShader);
        vertexShader = getShader(gl, shaders.VPhongShader);
    }


    shaderProgram = gl.createProgram();
//attach defined shaders to global WebGL shaders
    gl.attachShader(shaderProgram, vertexShader);
    gl.attachShader(shaderProgram, fragmentShader);
//link it to the programm
    gl.linkProgram(shaderProgram);

    if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
        alert("Could not initialise shaders");
    }

//use the defined shaders
    gl.useProgram(shaderProgram);

//attributes

//positions
    shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");
    gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

//texture
    shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");
    gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);

//normals
    shaderProgram.normals = gl.getAttribLocation(shaderProgram, "aNormal");
    gl.enableVertexAttribArray(shaderProgram.normals);


//uniforms for shader...
//Vertex Uniforms
    shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
    shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
    shaderProgram.NormalMatrixUniform = gl.getUniformLocation(shaderProgram, "uNormalMat");
//Light & koeffizent
    shaderProgram.ukdUniform = gl.getUniformLocation(shaderProgram, "uKd");
    shaderProgram.uksUniform = gl.getUniformLocation(shaderProgram, "uKs");
    shaderProgram.uShininessValUniform = gl.getUniformLocation(shaderProgram, "uShininessVal");
    shaderProgram.uDiffuseColorUniform = gl.getUniformLocation(shaderProgram, "uDiffuseColor");
    shaderProgram.uSpecularColorUniform = gl.getUniformLocation(shaderProgram, "uSpecularColor");
    shaderProgram.uLightPosrUniform = gl.getUniformLocation(shaderProgram, "uLightPos");
//Fragment Uniforms

    shaderProgram.samplerUniform = gl.getUniformLocation(shaderProgram, "uSampler");
//Light chooser uMode
    shaderProgram.uMode= gl.getUniformLocation(shaderProgram, "uMode");


}