let textureCube;
let textureCoord;
let Textureloaded = false;

function initTexture() {
    textureCube = gl.createTexture();
    textureCoord = gl.createTexture();
    textureCoord.image = new Image();
    textureCube.image = new Image();

    textureCube.image.onload = function () {
        handleLoadedTexture(textureCube);
        handleLoadedTexture(textureCoord);

        Textureloaded = true;
    };

    textureCube.image.src = "textures/texture1.gif";
    textureCoord.image.src = "textures/textureCoord.gif"
}

function handleLoadedTexture(texture) {
    gl.bindTexture(gl.TEXTURE_2D, texture);
    gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);
    gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
    gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR_MIPMAP_NEAREST);
    gl.generateMipmap(gl.TEXTURE_2D);
    gl.bindTexture(gl.TEXTURE_2D, null);
}
