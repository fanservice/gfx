class Torso {
    constructor(id, parsedOBJ, xyz) {
        this.id = id;
        this.VertexPositionBuffer = gl.createBuffer();
        this.VertexIndexBuffer = gl.createBuffer();
        this.VertexTextureCoordBuffer = gl.createBuffer();
        this.VertexNormalBuffer = gl.createBuffer();
        this.xyz = xyz;
        this.parsedOBJ = parsedOBJ;
        this.setBuffer(this.parsedOBJ.torso);
        this.objectM = [];
        this.init();
        this.position();

        this.setBody(this.xyz);

    }

    setBody() {
        this.head = new Head('head', this.parsedOBJ, [0, 2.6, 0]);
        this.armJointL = new UpperJoint('armJointL', this.parsedOBJ, [-0.555, 1.2, 0]);
        this.armJointR = new UpperJoint('armJointR', this.parsedOBJ, [0.555, 1.2, 0]);
        this.thighL= new Thigh('armJointL', this.parsedOBJ, [-0.4, -.8, 0]);
        this.thighR= new Thigh('armJointR', this.parsedOBJ, [0.3, -.8, 0]);




    }

    setBuffer(torso) {
        //coordinates
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
        let verticesArray = torso.vertices;
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(verticesArray), gl.STATIC_DRAW);
        this.VertexPositionBuffer.itemSize = 3;
        this.VertexPositionBuffer.numItems = torso.vertices.length / this.VertexPositionBuffer.itemSize;


        //Indices
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.VertexIndexBuffer);
        let indexArray = torso.VertexIndices;
        gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indexArray), gl.STATIC_DRAW);
        this.VertexIndexBuffer.itemSize = 1;
        this.VertexIndexBuffer.numItems = torso.VertexIndices.length;

        //texture
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexTextureCoordBuffer);
        let textureArray = torso.vertexTextureOrdered;
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(textureArray), gl.STATIC_DRAW);
        this.VertexTextureCoordBuffer.itemSize = 2;
        this.VertexTextureCoordBuffer.numItems = torso.vertexTextureOrdered.length / this.VertexTextureCoordBuffer.itemSize;

        //normals
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexNormalBuffer);
        let normals = torso.vertexNormalsOrdered;
        gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(normals), gl.STATIC_DRAW);
        this.VertexNormalBuffer.itemSize = 3;
        this.VertexNormalBuffer.numItems = torso.vertexNormalsOrdered.length / this.VertexTextureCoordBuffer.itemSize;


    }


    setCoordinateSystem(boolean) {
        this.CoordinateSystemActive = boolean;
    }

    init() {
        mat4.identity(this.objectM);

    };


    rotate(x, y, z, denominatorOfPI) {

        mat4.rotate(this.objectM, this.objectM, Math.PI / denominatorOfPI, [x, y, z]);
    }


    position() {

        mat4.translate(this.objectM, this.objectM, this.xyz);

    }

    move(x, y, z) {

        mat4.translate(this.objectM, this.objectM, [x, y, z]);
    }

    scale(x, y, z) {
        mat4.scale(this.objectM, this.objectM, [x, y, z]);
    }

    debuggingLogger(parrent) {

        console.log("Cube ObjecttM  " + this.id + "\n" + this.objectM);
        console.log("Parrent of" + this.id + "\n" + parrent);
        console.log("Mmatrix of" + this.id + "\n" + World);
    }


    setDebugger(boolean) {
        this.debugger = false;
    }

//to the graphiccard
    render() {

        mat4.copy(mVMatrix, globalM);


        mat4.multiply(mVMatrix, mVMatrix, this.objectM);
        mat3.normalFromMat4(Normal, mVMatrix);


        //Vertex
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexPositionBuffer);
        gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.VertexIndexBuffer);
        gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.VertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);
        //Texture
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexTextureCoordBuffer);

        gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.VertexTextureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);
        gl.activeTexture(gl.TEXTURE0);
        gl.bindTexture(gl.TEXTURE_2D, textureCube);
        gl.uniform1i(shaderProgram.samplerUniform, 0);

        //Normals
        gl.bindBuffer(gl.ARRAY_BUFFER, this.VertexNormalBuffer);

        gl.vertexAttribPointer(shaderProgram.normals, this.VertexNormalBuffer.itemSize, gl.FLOAT, false, 0, 0);

        setMatrixUniforms();
        gl.drawElements(gl.TRIANGLES, this.VertexIndexBuffer.numItems, gl.UNSIGNED_SHORT, 0);

        this.head.render(mVMatrix);
        this.armJointL.render(mVMatrix);
        this.armJointR.render(mVMatrix);
        this.thighR.render(mVMatrix);
        this.thighL.render(mVMatrix);



    }
}