
let shaders = {
    FGaroudShader: {
        shader: "precision mediump float;\n" +
            " uniform sampler2D uSampler;\n" +
            " varying vec2 vTextureCoord;\n" +
            " varying vec4 vColor;\n" +
            "\n" +
            " void main() {\n" +
            "      gl_FragColor =vColor* texture2D(uSampler, vec2(vTextureCoord.x,vTextureCoord.y));\n" +
            "\n" +
            " }\n",
        name: "fs"
    },
    VGaroudShader:
        {
            shader: "//attributes\n" +
                "attribute vec3 aNormal;\n" +
                "attribute vec3 aVertexPosition;\n" +
                "attribute vec2 aTextureCoord;\n" +
                "//uniform\n" +
                "uniform mat4 uPMatrix, uMVMatrix;\n" +
                "uniform mat3 uNormalMat;\n" +
                "uniform float uKd;   // Diffuse koeffizient\n" +
                "uniform float uKs;   // Specular  koeffizient\n" +
                "uniform float uShininessVal; // Shininess\n" +
                "uniform int uMode;\n" +
                "// Material color\n" +
                "uniform vec3 uDiffuseColor;\n" +
                "uniform vec3 uSpecularColor;\n" +
                "uniform vec3 uLightPos; // Light position\n" +
                "//varying\n" +
                "varying vec4 vColor; //color\n" +
                "varying vec3 vNormalInterp;\n" +
                "varying vec3 vVertPos;\n" +
                "varying vec2 vTextureCoord;\n" +
                "\n" +
                "\n" +
                "//\n" +
                "void main(){\n" +
                "\n" +
                "  vec4 vertPos4 =uMVMatrix * vec4(aVertexPosition, 1.0);\n" +
                "  vTextureCoord = aTextureCoord;\n" +
                "  vVertPos = vec3(vertPos4) / vertPos4.w;\n" +
                "  vNormalInterp =uNormalMat *aNormal ;\n" +
                "  gl_Position = uPMatrix * vertPos4;\n" +
                "  //\n" +
                "  vec3 N = normalize(vNormalInterp);\n" +
                "  vec3 L = normalize(uLightPos - vec3(uMVMatrix));\n" +
                "  // Lambert's cosine law\n" +
                "  float lambertian = max(dot(N, L), 0.0);\n" +
                "  float specular = 0.0;\n" +
                "  if(lambertian >= 0.0) {\n" +
                "    vec3 R = reflect(-L, N);      // Reflected Light vector\n" +
                "    vec3 V = normalize(-vVertPos); // Vector to viewer\n" +
                "    // Compute the specular term\n" +
                "    float specAngle = max(dot(R, V), 0.0);\n" +
                "    specular = pow(specAngle, uShininessVal);\n" +
                "  //modes\n" +
                "  //0 =all\n" +
                "  //1=diffuse\n" +
                "  //2=specular\n" +
                "  if(uMode==0)\n" +
                "  {\n" +
                "  vColor = vec4(\n" +
                "               vec3 (.7,.7,.7)+uKd * lambertian * uDiffuseColor +\n" +
                "               uKs * specular * uSpecularColor, 1.0);\n" +
                "}else if(uMode==1)\n" +
                "{\n" +
                "  vColor = vec4(uKd * lambertian * uDiffuseColor,1.0);\n" +
                "}else if(uMode==2)\n" +
                "{\n" +
                "  vColor = vec4(uKs * specular * uSpecularColor, 1.0);\n" +
                "\n" +
                "}else{\n" +
                "\n" +
                " vColor = vec4(1.0,1.0,1.0, 1.0);\n" +
                "}\n" +
                "}else{\n" +
                "vColor = vec4(1.0,1.0,1.0, 1.0);\n" +
                "}}",
            name: "vs"
        },
    FPhongShader: {
        shader: "precision mediump float;\n" +
            "//uniforms\n" +
            "uniform sampler2D uSampler;\n" +
            "uniform float uKd;   // Diffuse koeffizient\n" +
            "uniform float uKs;   // Specular  koeffizient\n" +
            "uniform float uShininessVal; // Shininess\n" +
            "uniform int uMode;\n" +
            "// Material color\n" +
            "uniform vec3 uDiffuseColor;\n" +
            "uniform vec3 uSpecularColor;\n" +
            "uniform vec3 uLightPos; // Light position\n" +
            "//varying\n" +
            "varying vec2 vTextureCoord;\n" +
            "varying vec3 vNormalInterp;  //\n" +
            "varying vec3 vVertexPosition;       // Vertex position\n" +
            "varying vec3 worldPosition;\n" +
            "void main() {\n" +
            "\n" +
            "  vec3 N = normalize(vNormalInterp);\n" +
            "  vec3 L = normalize(uLightPos - worldPosition);\n" +
            "  // Lambert's cosine law\n" +
            "  float lambertian = max(dot(N, L), 0.0);\n" +
            "  float specular = 0.0;\n" +
            "  if(lambertian >= 0.0) {\n" +
            "    vec3 R = reflect(-L, N);      // Reflected Light vector\n" +
            "    vec3 V = normalize(-vVertexPosition); // Vector to viewer\n" +
            "    // Compute the specular term\n" +
            "    float specAngle = max(dot(R, V), 0.0);\n" +
            "    specular = pow(specAngle, uShininessVal);\n" +
            "    if(uMode==0)\n" +
            "    {\n" +
            "    vec4 light=  vec4(uKd * lambertian * uDiffuseColor + uKs * specular * uSpecularColor, 1.0);\n" +
            "      gl_FragColor =light* texture2D(uSampler, vec2(vTextureCoord.s,vTextureCoord.t));\n" +
            "      }else if(uMode==1)\n" +
            "      {\n" +
            "      vec4 light=  vec4(uKd * lambertian * uDiffuseColor , 1.0);\n" +
            "            gl_FragColor =light* texture2D(uSampler, vec2(vTextureCoord.s,vTextureCoord.t));\n" +
            "      }else if(uMode==2)\n" +
            "      {\n" +
            "      vec4 light=vec4(uKs * specular * uSpecularColor,1.0);\n" +
            "                  gl_FragColor = texture2D(uSampler, vec2(vTextureCoord.s,vTextureCoord.t))*light;\n" +
            "\n" +
            "      }else {\n" +
            "      gl_FragColor = texture2D(uSampler, vec2(vTextureCoord.s,vTextureCoord.t));\n" +
            "      }\n" +
            "  }else{gl_FragColor =texture2D(uSampler, vec2(vTextureCoord.s,vTextureCoord.t));}\n" +
            "\n" +
            "}\n",
        name: "fs"
    },
    VPhongShader:
        {
            shader: "//Phong\n" +
                "//attribute\n" +
                "attribute vec3 aVertexPosition;\n" +
                "attribute vec2 aTextureCoord;\n" +
                "attribute vec3 aNormal;\n" +
                "//unis\n" +
                "uniform mat4 uPMatrix, uMVMatrix;\n" +
                "uniform mat3 uNormalMat;\n" +
                "//varies\n" +
                "varying vec3 vNormalInterp;\n" +
                "varying vec3 vVertexPosition;\n" +
                "varying vec2 vTextureCoord;\n" +
                "varying vec3 worldPosition;\n" +
                "\n" +
                "\n" +
                "\n" +
                "void main(){\n" +
                "  vec4 vertPos4 = uMVMatrix * vec4(aVertexPosition, 1.0);\n" +
                "  worldPosition=vec3(uMVMatrix);\n" +
                "  vVertexPosition = vec3(vertPos4) / vertPos4.w;\n" +
                "  vNormalInterp = uNormalMat * aNormal;\n" +
                "  vTextureCoord = aTextureCoord;\n" +
                "  gl_Position = uPMatrix * vertPos4;\n" +
                "}\n",
            name: "vs"
        }
};

