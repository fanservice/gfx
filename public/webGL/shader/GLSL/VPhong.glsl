//Phong
//attribute
attribute vec3 aVertexPosition;
attribute vec2 aTextureCoord;
attribute vec3 aNormal;
//unis
uniform mat4 uPMatrix, uMVMatrix;
uniform mat3 uNormalMat;
//varies
varying vec3 vNormalInterp;
varying vec3 vVertexPosition;
varying vec2 vTextureCoord;
varying vec3 worldPosition;



void main(){
  vec4 vertPos4 = uMVMatrix * vec4(aVertexPosition, 1.0);
  worldPosition=vec3(uMVMatrix);
  vVertexPosition = vec3(vertPos4) / vertPos4.w;
  vNormalInterp = uNormalMat * aNormal;
  vTextureCoord = aTextureCoord;
  gl_Position = uPMatrix * vertPos4;
}
