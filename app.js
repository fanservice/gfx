var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
let parser= require('./public/parser/parser');

let sculpture={};

sculpture={

    torso:parser.parser("torso.obj","torso"),
    head:parser.parser("head.obj","head"),
    joint:parser.parser("joint.obj","joint"),
    arm:parser.parser("arm.obj","arm"),
    thigh:parser.parser("thigh.obj","thigh"),
};

let app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


app.use(express.static('public'));

app.get('/',function (req,res) {
    res.redirect("/webGL/index.html");

});
app.get('/obj/torso',function (req,res) {
    res.setHeader('Content-Type', 'application/json');
    res.send(sculpture);

});

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
