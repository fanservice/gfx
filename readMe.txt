# GFX

# Installation

please download and install node for server side
please use latest chrome or firefox
###commandline:
* npm install
* npm start
* url:   http://localhost:3000 you should be redirected to http://localhost:3000/webGL/index.html

# Notes

## 1b
I got some "race conditions" somewhere which i coudn't figure out just in time, sometimes reloading helps,
or in the case of shaders just press the button again, the texture is also not 100% perfect(colours are brighter...)

by default phong and all lights are activated just press the "key" to activate or deactivate the shader and light you want!

not sure if my parser 100% correct, normals from maya seem not the be the correct one for webgl?

texture for coordinate system is now working, sometimes "race condition" because of js and async,i try to fix it,

## 1a

Camera is mirrored(gespiegelt nicht sicher ob das der richtige term ist...) like in flight games so it's more realastic?
Only the inputs are changed form plus to minus and vice versa.

Global Coordinates are bound to globalView and is set to x=0,y=0,x-10, so the global koordinate 
center is at the center of the canvas.

I played around with textures, and tried to texture the coordinate system in a
different color, but i couldn't do it just in time, so the F should be a good
orientation till i figure it out how to do it.

All webgl files are in ./public/webGL

Jquery and Bootstraps js  are only used in index for beautifying
Using Jquerys Ajax to get the parsed file from the server.

# Sources


##obj
own obj are exported from Maya 

## Light
Part of the shader code is from this webste, i suited it to my old ones

http://www.cs.toronto.edu/~jacobson/phong-demo/

http://www.cs.brandeis.edu/~cs155/Lecture_16.pdf
https://www.opengl.org/discussion_boards/showthread.php/162199-Combining-texture-and-light
https://www.youtube.com/watch?v=eVFaKLuG_1Q
https://webgl2fundamentals.org/webgl/lessons/webgl-3d-lighting-directional.html
## WebGl
most of the code comes from here, especially Boilerplate code:
* http://learningwebgl.com (the site was down when i wrote this line. code is included in sources)

A lot of ideas comes from the https://webglfundamentals.org/ 

## texture
Dragonball from here
http://joke-battles.wikia.com/wiki/Dragon_Ball
Lektroball texture from here
https://www.pokewiki.de/images/thumb/4/44/Sugimori_101.png/247px-Sugimori_101.png(not used)
